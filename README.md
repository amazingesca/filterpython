# filterpython

Filter python

## Getting started

This program works about key word. You do not need run any parameters.

To run filter program:

    python3 filter.py

When you run this program, firstly, you will find an input where you must write your name before keep ahead.

Then, another input will appear where you need to put your Key Word to filter into server_list.txt.

When you write your key word, filter will activate and it will search if data exists inside of server list. If the data exists, it will save into filter.txt

You can use this program as many times as you want until you want to exit by typing the letter "N"

## Beware


The filter stores the information only once. When you start another search, the previous information found will be deleted to replace it with the new data


## Thanks for Using
## Agustin Scazzino - 2022
