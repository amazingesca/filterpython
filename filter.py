'''
Welcome to Amazingesca's filter


'''

# THIS PROGRAM IS KEY SENSITIVE
from distutils.log import error
from pdb import Restart
from queue import Empty
from subprocess import TimeoutExpired
import colorama
from colorama import Fore, Back, Style
colorama.init(autoreset=True)
import pygame
from pygame import mixer
from playsound import playsound
import signal
import multiprocessing




#init pygame

pygame.init()
clock = pygame.time.Clock()

# Background Sound

mixer.music.load('background-music.mp3')
pygame.mixer.music.set_volume(0.2) # set custom volume
mixer.music.play(-1)


# Input name

name=input(Fore.LIGHTGREEN_EX+ "Welcome! Please, inserte your name: ")
print("Welcome: " + name)
user="users.txt"
if name == "":
    print("Please, first insert your name to keep ahead")
    name=input(Fore.LIGHTGREEN_EX+ "Welcome! Please, inserte your name - 1 attempt left:  ")
if name == "":
    print(Fore.RED + "For security reasons, we can not start the filter.")
    print("Reason: No write your name.")
    exit()
else:
    with open('users.txt','r+') as writename:
        writename.write("Last User " + name )
    
    
#Filter Server List file        
def filterprogram():
    
    
    server_list = "server_list.txt" # File with the information
    output = "filter.txt" # Filter file
    searcher = input(Fore.GREEN + "Please, enter key word : ") # Input to write what u want to filter
    metadata= []
    with open(server_list, "r") as read_file:
        
        for line in read_file: # For to read serverlist 
            
            if searcher in line:        
                metadata.append("The data found was" + " - " + line )
                
    
    '''

    Print zone
    
    '''
    
    if searcher == "":
        print(Fore.CYAN + "Please, add any key word to continue")
        filterprogram()
    elif len(metadata) == 0:
            error = mixer.Sound('errorfound.mp3')
            error.play()
            print(Fore.LIGHTRED_EX + "Data no found")
    
    else:
        found = mixer.Sound('mariobroscoin.mp3')
        found.play()
        print(Fore.CYAN + "Data has been sent to Filter file.\nThanks for using this program! " + name)
    
    print(Fore.YELLOW + "The amount of elements filted were: ", len(metadata))

    '''
    Open, write & show filter information
    
    ''' 
    with open(output, "w") as output_file:
        for line in metadata:
            output_file.write(line + "\n")

    '''
    
    Start again
    
    '''
    
  
    Restart=input("Do you want start again. y/n: ").lower()
    if Restart == "y":
        filterprogram()
    elif Restart == "n":
        playsound('byebye.mp3')
        print("Thanks for using " + name)
        exit()
        
filterprogram() # When you write "Y", the filter start again




